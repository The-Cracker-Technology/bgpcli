rm -rf /opt/ANDRAX/bgp_cli
mkdir /opt/ANDRAX/bgp_cli

cd tcpmd5/

bash build.sh

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Build tcpmd5... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd ../

cp -Rf bgp_cli.py ipv6.py tcpmd5.so wordfile /opt/ANDRAX/bgp_cli

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
