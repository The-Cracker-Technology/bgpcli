#!/bin/sh

gcc -c -o tcpmd5.o tcpmd5.c `/opt/ANDRAX/python27/bin/python-config  --cflags` -fpic
ld -shared -soname tcpmd5.so -o tcpmd5.so -lc tcpmd5.o
strip tcpmd5.so

mv tcpmd5.so ../
