#! /usr/bin/env python

#       bgp_cli.py
#       
#       Copyright 2009 Daniel Mende <dmende@ernw.de>
#

#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#       
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#       
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import sys
import signal
import threading
import socket
import struct
import time
import cmd
import tcpmd5

BGP_CLI_VERSION = "v0.1.6"

BGP_PORT = 179

class bgp_msg(object):
    TYPE_OPEN = 1
    TYPE_UPDATE = 2
    TYPE_NOTIFICATION = 3
    TYPE_KEEPALIVE = 4
    
    def __init__(self, msg_type):
        self.type = msg_type

    def render(self, data):
        return ('\xff' * 16) +  struct.pack("!HB", len(data) + 19, self.type) + data

class bgp_open(bgp_msg):
    BGP_VERSION = 4
    
    def __init__(self, my_as, hold_time = 256, identity = socket.gethostbyname(socket.gethostname()), parameters = []):
        bgp_msg.__init__(self, self.TYPE_OPEN)
        self.my_as = my_as
        self.hold_time = hold_time
        self.identity = identity
        self.parameters = parameters

    def render(self):
        self.data = struct.pack("!BHH", self.BGP_VERSION, self.my_as, self.hold_time) + socket.inet_aton(self.identity)
        data = ""
        for x in self.parameters:
            data += x.render()
        if data:    self.data += struct.pack("!B", len(data))
        else:       self.data += '\x00'
        return bgp_msg.render(self, self.data + data)

class bgp_parameter(object):
    PARAMETER_CAPABILITY = 2

    def __init__(self, type):
        self.type = type

    def render(self, data = ""):
        a = struct.pack("!BB", self.type, len(data))
        return a + data

class bgp_capability(bgp_parameter):
    CAPABILITY_MP = 1
    CAPABILITS_ROUTE_REFRESH_1 = 2
    CAPABILITS_ROUTE_REFRESH_2 = 128

    def __init__(self, cap_type):
        bgp_parameter.__init__(self, bgp_parameter.PARAMETER_CAPABILITY)
        self.cap_type = cap_type

    def render(self, data = ""):
        a = struct.pack("!BB", self.cap_type, len(data))
        return bgp_parameter.render(self, a + data)

class bgp_capability_mp(bgp_capability):
    def __init__(self, af, sub_af):
        bgp_capability.__init__(self, bgp_capability.CAPABILITY_MP)
        self.af = af
        self.sub_af = sub_af
        
    def render(self):
        return bgp_capability.render(self, struct.pack("!HBB", self.af, 0, self.sub_af))

class bgp_update(bgp_msg):
    def __init__(self, wroutes, path_attr, nlri):
        bgp_msg.__init__(self, self.TYPE_UPDATE)
        self.wroutes = wroutes
        self.path_attr = path_attr
        self.nlri = nlri

    def render(self):
        w_data = ""
        for x in self.wroutes:
            w_data += x.render()
        w_data = struct.pack("!H", len(w_data)) + w_data
        p_data = ""
        for x in self.path_attr:
            p_data += x.render()
        p_data = struct.pack("!H", len(p_data)) + p_data
        n_data = ""
        for x in self.nlri:
            n_data += x.render()
        return bgp_msg.render(self, w_data + p_data + n_data)
        
class bgp_notification(bgp_msg):
    def __init__(self, err_code, err_sub, data = ""):
        bgp_msg.__init__(self, self.TYPE_NOTIFICATION)
        self.err_code = err_code
        self.err_sub = err_sub
        self.data = data
    
    def render(self):
        err = struct.pack("!BB", self.err_code, self.err_sub)
        return bgp_msg.render(self, err + self.data)

class bgp_keepalive(bgp_msg):
    def __init__(self):
        bgp_msg.__init__(self, self.TYPE_KEEPALIVE)

    def render(self):
        return bgp_msg.render(self, "")

class bgp_withdrawn_route(object):
    def __init__(self, length, prefix):
        self.length = length
        self.prefix = socket.inet_aton(prefix)

    def render(self):
        data = struct.pack("!B", self.length)
        for x in xrange(0, self.length / 8):
            data += self.prefix[x:x+1]
        return data

class bgp_path_attr(object):
    PATH_ATTR_ORIGIN = 1
    PATH_ATTR_AS_PATH = 2
    PATH_ATTR_NEXT_HOP = 3
    PATH_ATTR_MULTI_EXIT_DISC = 4
    PATH_ATTR_LOCAL_PREF = 5
    PATH_ATTR_MP_REACH_NLRI = 14
    PATH_ATTR_MP_UNREACH_NLRI = 14
    PATH_ATTR_EXTENDED_COMMUNITIES = 16
    
    def __init__(self, flags, type):
        self.flags = flags
        self.type = type

    def render(self, data):
        ret = struct.pack("!BBB", self.flags, self.type, len(data))
        return ret + data

class bgp_path_attr_origin(bgp_path_attr):
    ORIGIN_IGP = 0
    ORIGIN_EGP = 1
    ORIGIN_INCOMPLETE = 2
    
    def __init__(self, origin):
        bgp_path_attr.__init__(self, 0x40, self.PATH_ATTR_ORIGIN)
        self.origin = origin

    def render(self):
        return bgp_path_attr.render(self, struct.pack("!B", self.origin))

class bgp_as_path_segment(object):
    AS_PATH_AS_SET = 1
    AS_PATH_AS_SEQUENCE = 2
    
    def __init__(self, type, values):
        self.type = type
        self.values = values

    def render(self):
        data = struct.pack("!BB", self.type, len(self.values))
        for x in self.values:
            data += struct.pack("!H", x)
        return data

class bgp_path_attr_as_path(bgp_path_attr):
    def __init__(self, value):
        bgp_path_attr.__init__(self, 0x40, self.PATH_ATTR_AS_PATH)
        self.value = value

    def render(self):
        data = ""
        for x in self.value:
            data += x.render()
        return bgp_path_attr.render(self, data)
        
class bgp_path_attr_next_hop(bgp_path_attr):
    def __init__(self, next_hop):
        bgp_path_attr.__init__(self, 0x40, self.PATH_ATTR_NEXT_HOP)
        self.next_hop = socket.inet_aton(next_hop)

    def render(self):
        return bgp_path_attr.render(self, self.next_hop)

class bgp_path_attr_multi_exit_disc(bgp_path_attr):
    def __init__(self, multi_exit_disc):
        bgp_path_attr.__init__(self, 0x80, self.PATH_ATTR_MULTI_EXIT_DISC)
        self.multi_exit_disc = multi_exit_disc

    def render(self):
        return bgp_path_attr.render(self, struct.pack("!L", self.multi_exit_disc))

class bgp_path_attr_local_pref(bgp_path_attr):
    def __init__(self, local_pref):
        bgp_path_attr.__init__(self, 0x40, self.PATH_ATTR_LOCAL_PREF)
        self.local_pref = local_pref

    def render(self):
        return bgp_path_attr.render(self, struct.pack("!L", self.local_pref))

class bgp_extended_community(object):
    def __init__(self, encode, type, subtype, val1, val2 = "", val3 = ""):
        self.encode = encode
        self.type = type
        self.subtype = subtype
        self.val1 = val1
        self.val2 = val2
        self.val3 = val3

    def render(self):
        if self.encode == "two-octed":
            return struct.pack("!BBHL", self.type, self.subtype, self.val1, self.val2)
        if self.encode == "ipv4":
            data = struct.pack("!BB", self.type, self.subtype)
            data += socket.inet_aton(self.val1)
            data += struct.pack("!H", self.val2)
            return data
        if self.encode == "opaque":
            a = self.val1 % 256
            return struct.pack("!BBLH", self.type, self.subtype, self.val1 / 256, a)
        if self.encode == "ospf-domain":
            data = struct.pack("!BB", self.type, self.subtype)
            data += socket.inet_aton(self.val1)
            data += struct.pack("!BB", self.val2, self.val3)
            return data
        else:
            a = self.val1 % 4096
            b = a % 16
            return struct.pack("!BLHB", self.type, self.val1 / 4096, a / 16, b)

class bgp_path_attr_extended_communities(bgp_path_attr):
    def __init__(self, communities):
        bgp_path_attr.__init__(self, 0xc0, self.PATH_ATTR_EXTENDED_COMMUNITIES)
        self.communities = communities

    def render(self):
        data = ""
        for x in self.communities:
            data += x.render()
        return bgp_path_attr.render(self, data)

class bgp_mp_rfc3107_nlri(object):
    def __init__(self, length, stack, prefix):
        self.length = length
        self.stack = stack
        self.prefix = prefix
        
    def render(self):
        data = struct.pack("!B", self.length)
        x = self.stack.split(':')
        for y in xrange(0, len(x)):
            a = int(x[y]) % 16
            data += struct.pack("!H", int(x[y]) / 16)
            a *= 16
            if y == len(x) - 1:
                a += 1
            data += struct.pack("!B", a)
        b = self.prefix.split(':')
        data += struct.pack("!LL", int(b[0]), int(b[1]))
        data += socket.inet_aton(b[2])
        return data
    
class bgp_path_attr_mp_reach_nlri(bgp_path_attr):
    AF_IPV4 = 1
    SUB_AF_VPN = 128

    def __init__(self, type, next_hop, snpa, net_reach):
        bgp_path_attr.__init__(self, 0x80, self.PATH_ATTR_MP_REACH_NLRI)
        if type == "ipv4-mpls":
            self.af_id = self.AF_IPV4
            self.sub_af_id = self.SUB_AF_VPN
            self.addr_len = 12
        else:
            self.af_id = 0
            self.sub_af_id = 0
            self.addr_len = 0

        self.next_hop = next_hop
        self.snpa = snpa
        self.net_reach = net_reach

    def render(self):
        data = struct.pack("!HBB", self.af_id, self.sub_af_id, self.addr_len)
        a = self.next_hop.split(':')
        data += struct.pack("!LL", int(a[0]), int(a[1]))
        data += socket.inet_aton(a[2])
        data += struct.pack("!B", len(self.snpa))
        for x in self.snpa:
            data += x.render()
        for x in self.net_reach:
            data += x.render()
        return bgp_path_attr.render(self, data)

class bgp_path_attr_mp_unreach_nlri(bgp_path_attr):
    def __init__(self, type, wroutes):
        bgp_path_attr.__init__(self, 0x80, self.PATH_ATTR_MP_UNREACH_NLRI)
        if type == "ipv4-mpls":
            self.af_id = AF_IPV4
            self.sub_af_id = SUB_AF_VPN
        else:
            self.af_id = 0
            self.sub_af_id = 0
        self.wroutes = wroutes

    def render(self):
        data = struct.pack("!BH", self.af_id, self.sub_af_id)
        for x in self.wroutes:
            data += x.render()
        return bgp_path_attr.render(self, data)

class bgp_nlri(object):
    def __init__(self, length, prefix):
        self.length = length
        self.prefix = socket.inet_aton(prefix)

    def render(self):
        data = struct.pack("!B", self.length)
        for x in xrange(0, self.length / 8):
            data += self.prefix[x:x+1]
        return data

class bgp_listener(threading.Thread):
    def __init__(self):
        self.active = False
        threading.Thread.__init__(self)

    def run(self):
        self.poll()
        
    def poll(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        for (i, j) in interface.md5:
            tcpmd5.set(self.sock.fileno(), i, BGP_PORT, j)
        self.sock.bind(("0.0.0.0", BGP_PORT))
        self.sock.listen(1)
        while(self.active):
            try:
                (csock, addr) = self.sock.accept()
            except:
                continue
            print "incomming connection from " + addr.__str__()
            csock.settimeout(1)
            while(self.active):
                try:
                    read = csock.recv(4096)
                except:
                    continue
                if not read:    break
                print read
            csock.close()
            print "connection from " + addr.__str__() + " closed"
        self.sock.close()

class bgp_session(threading.Thread):
    def __init__(self, parameters, my_as, hold_time = 256, identity = socket.gethostbyname(socket.gethostname())):
        self.dest = None
        self.sock = None
        self.parameters = parameters
        self.my_as = my_as
        self.hold_time = hold_time
        self.identity = identity
        self.keepalive_msg = bgp_keepalive()
        self.active = False
        self.fuzz = False
        self.msg = None
        self.sem = threading.Semaphore()
        threading.Thread.__init__(self)

    def connect(self, dest, timeout = 3, bf = False):
        self.dest = dest
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(timeout)
        for (i, j) in interface.md5:
            tcpmd5.set(self.sock.fileno(), i, BGP_PORT, j)
            
        self.sock.connect((self.dest, BGP_PORT))
        if not bf:
            msg = bgp_open(self.my_as, self.hold_time, self.identity, self.parameters)
            self.sock.send(msg.render())
            self.active = True
        
    def disconnect(self):
        self.active = False
        if self.isAlive():
            self.join()
        self.sock.close()
       
    def update(self, msg):
        self.fuzz = False
        self.sem.acquire()
        self.msg = msg
        self.sem.release()
    
    def run(self):
        self.keepalive()
        print "Keepalive thread terminated"
        interface.session = None

    def keepalive(self):
        while(self.active):
            self.sem.acquire()
            try:
                self.sock.send(self.keepalive_msg.render())
                if self.msg:
                    self.sock.send(self.msg.render())
                    self.msg = None
            except socket.error:
                print "Connection interupted"
                self.active = False
            self.sem.release()
            time.sleep(self.hold_time / 4)
        self.sock.close()

class bgp_interface(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.intro = "BGP_CLI " + BGP_CLI_VERSION + " by Daniel Mende - dmende@ernw.de"
        self.prompt = "BGP_CLI> "
        self.session = None
        self.listener = None
        self.md5 = []
        self.msg = None
        self.parameters = []
        
    def do_md5bf(self, args):
        if not self.session:
            print "No session initialized. do this first"
        else:
            t = args.split()
            if len(t) >= 2:
                try:
                    wl = open(t[1])
                except Exception, e:
                    print "Can't open wordfile: " + e.__str__()
                    return
                for i in wl:
                    self.md5 = [ (t[0], i.strip()) ]
                    try:
                        if len(t) == 3:
                            self.session.connect(t[0], int(t[2]), bf=True)
                        else:
                            self.session.connect(t[0], bf=True)
                    except socket.timeout:
                        print "next pw: " + i.strip()
                        self.session.disconnect()
                        continue
                    except:
                        return
                    print "Found pw '" + i.strip() + "' for host " + t[0]
                    self.session.disconnect()
                    break
            else:
                print "Wrong parameter count"

    def help_md5bf(self):
        print "Do brute force for md5 secret ARGS: TARGET WORDFILE [TIMEOUT = 3]"

    def do_fuzz(self, args):
        if not self.session:
            print "No session initialized. do this first"
        else:
            if self.session.hold_time > 2:
                print "The hold timer in your session is bigger than 2 seconds. This might leed to transmission problems"
            t = args.split()
            if len(t) >= 2:
                if len(t) == 3:
                    sf = open(t[2])
                else: 
                    sf = None
                
                for i in xrange(int(t[1]), 255):
                    sys.stdout.write("\n" + i.__str__())
                    for j in xrange(0, 255):
                        try:
                            sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            sock.settimeout(1)
                            sock.connect((t[0], BGP_PORT))
                            msg = bgp_open(self.session.my_as, self.session.hold_time, self.session.identity)
                            sock.send(msg.render(None))
                            if sf:
                                for k in sf:
                                    notifi = bgp_notification(i, j, k)
                                    sock.send(notifi.render())
                            else:
                                notifi = bgp_notification(i, j)
                                sock.send(notifi.render())
                            sock.close()
                            time.sleep(1)
                        except:
                            if self.session:
                                return
                            sys.stdout.write("!")
                        else:
                            sys.stdout.write(".")
                        sys.stdout.flush()
                print "done"
            else:
                print "Wrong parameter count"
    
    def help_fuzz(self):
        print "Fuzzes with bgp errors ARGS: IP START_AT [STRINGFILE]"

    def do_md5(self, args):
        t = args.split()
        if len(t) == 2:
            self.md5.append((t[0], t[1]))
        elif len(t) == 1:
            md5 = []
            for (i, j) in self.md5:
                if not i == t[0]:
                    md5.append((i, j))
            self.md5 = md5
        else:
            print "Wrong parameter count"
        #print self.md5

    def help_md5(self):
        print """Sets md5 parameters for RFC 2385 signing. ARGS: IP [PASSWD]
To disable signing use empty password."""

    def do_session(self, args):
        if self.session:
            print "Session already open. close it first."
        else:
            t = args.split()
            if len(t) == 1:
                self.session = bgp_session(self.parameters, int(t[0]))
            elif len(t) == 2:
                self.session = bgp_session(self.parameters, int(t[0]), int(t[1]))
            elif len(t) == 3:
                self.session = bgp_session(self.parameters, int(t[0]), int(t[1]), t[2])
            else:
                print "Wrong parameter count"
                return
            print "Session created"

    def help_session(self):
        print """Init BGP session. ARGS: MY_AS [HOLD_TIME = 256] [IDENT]
to add optional parameters set the self.parameters list.

available objects are:

bgp_parameter(type)
bgp_capability(cap_type)
bgp_capability_mp(af, sub_af)
"""

    def do_listen(self, args):
        if self.listener:
            print "Listener already running. close it first."
        else:
            self.listener = bgp_listener()
            self.listener.active = True
            self.listener.start()

    def help_listen(self):
        print "Listen for incomming BGP connections"
    
    def do_connect(self, args):
        if not self.session:
            print "No session initialized. do this first"
        else:
            try:
                self.session.connect(args)
            except Exception, e:
                print "Can't connect: " + e.__str__()
                return
            print "Connected"
            try:
                self.session.start()
            except Exception, e:
                print "Can't start thread: " + e.__str__()
                return
            print "Keepalive thread started"

    def help_connect(self):
        print "Connect to a BGP speaking gateway. ARGS: IP"

    def do_disconnect(self, args):
        if not self.session:
            print "No session initialized"
        else:
            if not self.session.active:
                print "Not connected"
            else:
                self.session.disconnect()

    def help_disconnect(self):
        print "Disconnect a open connection"

    def do_EOF(self, arg):
        print ""
        self.do_exit("")
    
    def do_exit(self, args):
        print "Bye..."
        if interface.session:
            interface.session.disconnect()
        if interface.listener:
            interface.listener.active = False
            interface.listener.join()
        sys.exit(0)

    def help_exit(self):
        print "Quits BGP_CLI"

    def emptyline(self):
        pass

    def default(self, line):       
        try:
            exec(line)
        except Exception, e:
            print e.__class__, ":", e

    def preloop(self):
        cmd.Cmd.preloop(self)
        self._hist = []
        self._locals = {}
        self._globals = {}

    def do_hist(self, args):
        print self._hist

    def help_hist(self):
        print "Prints the command history"

    def precmd(self, line):
        self._hist += [line.strip()]
        return line

    def do_update(self, args):
        if not self.session:
            print "No session initialized"
        else:
            if not self.session.active:
                print "Not connected"
            else:
                if self.msg:
                    self.session.update(self.msg)
                else:
                    print "No msg prepared. See help"
                print "Update sent"

    def help_update(self):
        print """sends a prepared update message.
to prepare an update message set the self.msg object.

available objects are:

bgp_update(w_routes, path_attr, nlri)
bgp_withdrawn_route(length, prefix)
bgp_path_attr_origin(origin)
bgp_path_attr_as_path(value)
bgp_as_path_segment(type, values)
bgp_path_attr_next_hop(next_hop)
bgp_path_attr_multi_exit_disc(multi_exit_disc)
bgp_path_attr_local_pref(local_pref)
bgp_path_attr_extended_communities(communities)
bgp_extended_community(encode, type, subtype, val1, val2, val3)
bgp_path_attr_mp_reach_nlri(type, next_hop, snpa, net_reach)
bgp_path_attr_mp_unreach_nlri(type)
bgp_mp_rfc3107_nlri(length, stack, prefix)
bgp_nlri(length, prefix)
bgp_notification(err_code, err_sub, data)

example:
self.msg = bgp_update([], [bgp_path_attr_origin(0), bgp_path_attr_as_path([bgp_as_path_segment(2, [2])]), bgp_path_attr_next_hop("192.168.1.77")], [bgp_nlri(8,"10.0.0.0")])

self.msg = bgp_update([], [bgp_path_attr_origin(2), bgp_path_attr_as_path([]), bgp_path_attr_multi_exit_disc(0), bgp_path_attr_local_pref(100), bgp_path_attr_extended_communities([bgp_extended_community("two-octed", 0x00, 0x02, 65534, 1), bgp_extended_community("ospf-domain", 0x00, 0x05, "0.0.0.100", 2, 0), bgp_extended_community("ospf-domain", 0x80, 0x00, "0.0.0.0", 2, 0), bgp_extended_community("ipv4", 0x80, 0x01, "192.168.254.2", 0)]), bgp_path_attr_mp_reach_nlri("ipv4-mpls", "0:0:192.168.1.77", [], [bgp_mp_rfc3107_nlri(120, "34", "2:1:10.0.0.1")])], [])
"""

    def help_help(self):
        print """BGP_CLI> session 2 8
Session created
BGP_CLI> connect 192.168.178.3
Connected
Keepalive thread started
BGP_CLI> self.msg = bgp_update([], [bgp_path_attr_origin(0), bgp_path_attr_as_path([bgp_as_path_segment(2, [2])]), bgp_path_attr_next_hop("192.168.1.77")], [bgp_nlri(8,"10.0.0.0")])
BGP_CLI> update
BGP_CLI>   
Bye...

BGP_CLI> session 3 8 
Session created
BGP_CLI> md5bf 192.168.178.3 wordfile
next pw: hallo

next pw: test

Found pw 'test456' for host 192.168.178.3
BGP_CLI> exit
Bye...
"""

if __name__ == "__main__":

    def sigint(signum, frame):
        if interface.session:
            interface.session.disconnect()
        if interface.listener:
            interface.listener.active = False
            interface.listener.join()

    signal.signal(signal.SIGINT, sigint)

    interface = bgp_interface()
    interface.cmdloop()

#session 2 8
#connect 192.168.1.3
#self.msg = bgp_update([], [bgp_path_attr_origin(0), bgp_path_attr_as_path([bgp_as_path_segment(2, [2])]), bgp_path_attr_next_hop("192.168.1.77")], [bgp_nlri(8,"10.0.0.0")])
#update
